﻿using Advantage.Data.Provider;
using MedisoftEMRIntegration.Core.Entities;
using MedisoftEMRIntegration.Infrastructure.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Infrastructure.Context
{
    public class AppointmentContext
    {
        AdsCommand cmd;

        public AppointmentContext()
        {
            /////logging controllers
        }


        //    public List<Appointment> GeAppointment()
        //    {
        //        string query = "select TOP 3 id,date,[start Time] time,length,Provider,[Chart Number] chart_number,[Case Number] case_number,"
        //               + " trim(status) status,[reason code] type,[Check In Time] inTime,[Check Out Time] outTime,note"
        //               + " from OHAPP_TAR WHERE [Migration Status] =  'NM'";

        //        AdsDataReader reader = null;

        //        using (AdsConnection conn = new AdsConnection(DatabaseSettings.DatabaseConnection))
        //        {
        //            try

        //            {
        //                conn.Open();
        //                cmd = conn.CreateCommand();
        //                cmd.CommandText = query;
        //                reader = cmd.ExecuteReader();
        //                if (reader.HasRows)
        //                {
        //                    Debug.WriteLine("test");
        //                    List<Appointment> appointment = new List<Appointment>();
        //                    if (reader != null)
        //                    {

        //                        while (reader.Read())
        //                        {

        //                            appointment.Add(new Appointment
        //                            {
        //                                AppointmentId = int.Parse(reader["id"].ToString()),
        //                                AppointmentDate = reader["date"].ToString(),
        //                                Duration = reader["length"].ToString(),
        //                                //provider = reader["Provider"].ToString(),
        //                                //chart_number = reader["chart_number"].ToString(),
        //                                CaseNumber = int.Parse(reader["case_number"].ToString()),
        //                                Status = reader["status"].ToString(),
        //                                Type = reader["type"].ToString(),
        //                                CheckInTime = (!string.IsNullOrEmpty(reader["inTime"].ToString()) ? DateTime.Parse(reader["inTime"].ToString()) : DateTime.Now),
        //                                CheckOutTime = (!string.IsNullOrEmpty(reader["outTime"].ToString()) ? DateTime.Parse(reader["outTime"].ToString()) : DateTime.Now),
        //                                Notes = reader["note"].ToString(),
        //                                requestedPeriod = new List<RequestedPeriod>
        //                               {
        //                                   new RequestedPeriod{
        //                                   end="",
        //                                   start = reader["time"].ToString()
        //                                   }
        //                               }

        //                            });

        //                        }
        //                        conn.Close();
        //                        return appointment;
        //                    }

        //                    else
        //                    {
        //                        return null;
        //                    }
        //                }
        //            }

        //            catch (AdsException ex)
        //            {
        //                //_lgc.Log("Fetch_all_appointments_", ex.Message);
        //                return null;
        //            }
        //            catch (SystemException ex)
        //            {
        //                //_lgc.Log("Fetch_all_appointments_", ex.Message);
        //                return null;
        //            }
        //            return null;
        //        }
        //    }
        //    public bool updateMigrationStatus(List<Appointment> lst)
        //    {
        //        List<int> Identierfiers = new List<int>();
        //        foreach (var pat in lst)
        //        {
        //            Identierfiers.Add(pat.id);
        //        }
        //        string joined = string.Join(",", Identierfiers);
        //        Debug.WriteLine(joined);

        //        string query = "UPDATE OHAPP_TAR SET OHAPP_TAR.[Migration Status] = 'M' FROM OHAPP_TAR WHERE OHAPP_TAR.[ID] IN (" + joined + ")";
        //        Debug.WriteLine(query);
        //        AdsDataReader reader = null;
        //        using (AdsConnection conn = new AdsConnection(DatabaseSettings.DatabaseConnection))
        //        {
        //            try

        //            {
        //                conn.Open();
        //                cmd = conn.CreateCommand();
        //                cmd.CommandText = query;
        //                reader = cmd.ExecuteReader();

        //                conn.Close();
        //                return true;
        //            }

        //            catch (AdsException ex)
        //            {
        //                _lgc.Log("Data Migration Status(Appointments)_", ex.Message);
        //            }
        //        }

        //        return false;
        //    }
        //}
    }
}
