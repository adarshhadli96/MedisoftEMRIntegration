﻿using MedisoftEMRIntegration.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Infrastructure.Interfaces
{
    public interface IInsuranceRepository
    {
        public bool insert(Insurance insurance);

        public List<Insurance> findAllInsurance();
    }
}
