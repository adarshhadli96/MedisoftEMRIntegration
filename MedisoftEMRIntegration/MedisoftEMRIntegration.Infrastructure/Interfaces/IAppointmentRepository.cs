﻿using MedisoftEMRIntegration.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Infrastructure.Interfaces
{
    public interface IAppointmentRepository
    {
        public bool insert(Appointment appointment);

        public List<Appointment> GetAllAppointment();
    }
}
