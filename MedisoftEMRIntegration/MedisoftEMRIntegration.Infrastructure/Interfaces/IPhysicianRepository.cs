﻿using MedisoftEMRIntegration.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Infrastructure.Interfaces
{
    public interface IPhysicianRepository
    {
        public bool insert(Physician physician);

        public List<Physician> findAllPhysician();
    }
}
