﻿using MedisoftEMRIntegration.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Infrastructure.Interfaces
{
    public interface IFacilityRepository
    {
        public bool insert(Facility facility);

        public List<Facility> findAllFacility();
    }
}
