﻿using MedisoftEMRIntegration.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Infrastructure.Interfaces
{
    public interface IGurantorRepository
    {
        public bool insert(Gurantor gurantor);

        public List<Gurantor> findAllGurantor();
    }
}
