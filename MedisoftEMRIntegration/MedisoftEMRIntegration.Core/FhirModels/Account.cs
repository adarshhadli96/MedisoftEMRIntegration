﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Core.FhirModels
{
    public class Account
    {
        public string Identifier { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Type Type { get; set; }
        public List<Subject> Subject { get; set; }
        public ServicePeriod ServicePeriod { get; set; }
        public List<Coverage> Coverage { get; set; }
        public Owner Owner { get; set; }
        public List<Guarantor> Guarantor { get; set; } 

    }

    public class Type
    {
        public Coding Coding { get; set; }
        public string Text { get; set; }
    }

    public class Subject
    {
        public string Reference { get; set; }
        public string Display { get; set; }
    }

    public class ServicePeriod
    {
        public string Start { get; set; }
        public string End { get; set; }
    }

    public class Owner
    {
        public string Reference { get; set; }
        public string Display { get; set; }
    }

    public class Guarantor
    {
        public Party Party { get; set; }
        public string OnHold { get; set; }
        public Period Period { get; set; }
    }

    public class Coding
    {
        public string Code { get; set; }
        public string Display { get; set; }
    }

    public class Party
    {
        public string Reference { get; set; }
        public string Display { get; set; }
    }


    public class Period
    {
        public string Start { get; set; }
    }
}
