﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Core.Entities
{
    public class Insurance
    {
        public int ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAddress1 { get; set; }
        public string ProviderAddress2 { get; set; }
        public string ProviderCity { get; set; }
        public string ProviderState { get; set; }
        public int ProviderZip { get; set; }
        public string ProviderPhone { get; set; }
        public bool InsuredPerson { get; set; }
        public int PolicyNumber { get; set; }
        public int GroupNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Relationship { get; set; }
        public int Order { get; set; }
    }
}
