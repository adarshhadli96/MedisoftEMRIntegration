﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedisoftEMRIntegration.Core.Entities
{
    public class Physician
    {
        public int PhysicianId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public string HPhone { get; set; }
        public string Fax { get; set; }
        public int Ssn { get; set; }
        public string License { get; set; }
        public int DeaNumber { get; set; }
        public string Email { get; set; }
        public int Npi { get; set; }
        public string WPhone { get; set; }
        public string Mobile { get; set; }
        public string LoginName { get; set; }
        public string Speciality { get; set; }
        public string Type { get; set; }
    }
}
